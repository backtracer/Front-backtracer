import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkerUserPositionComponent } from './marker-user-position.component';

describe('MarkerUserPositionComponent', () => {
  let component: MarkerUserPositionComponent;
  let fixture: ComponentFixture<MarkerUserPositionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarkerUserPositionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkerUserPositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
