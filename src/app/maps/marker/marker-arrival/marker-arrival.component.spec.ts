import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkerArrivalComponent } from './marker-arrival.component';

describe('MarkerArrivalComponent', () => {
  let component: MarkerArrivalComponent;
  let fixture: ComponentFixture<MarkerArrivalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarkerArrivalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkerArrivalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
