import { Component, Input, OnInit } from '@angular/core';
import {RouteTypesService} from "../../services/data/route-types/route-types.service";

@Component({
  selector: 'app-marker',
  templateUrl: './marker.component.html',
  styleUrls: ['./marker.component.css']
})
export class MarkerComponent implements OnInit {

  @Input()
  shortName : string = ""

  @Input()
  color : string = "white"

  @Input()
  colorText : string = "dark"

  @Input()
  routeTypeId: number = 0

  constructor(
    public routeTypesService: RouteTypesService,
  ) {}

  ngOnInit(): void {}
}
