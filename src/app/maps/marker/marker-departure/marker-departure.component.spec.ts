import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkerDepartureComponent } from './marker-departure.component';

describe('MarkerDepartureComponent', () => {
  let component: MarkerDepartureComponent;
  let fixture: ComponentFixture<MarkerDepartureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarkerDepartureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkerDepartureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
