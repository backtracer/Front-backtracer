import {DOCUMENT} from '@angular/common';
import {AfterViewInit, Component, ElementRef, Inject, ViewChild} from '@angular/core';
import {
  circleMarker,
  Control,
  divIcon, DomEvent,
  geoJSON,
  latLng,
  LatLng,
  Layer,
  LayerGroup,
  Map as MapLeaflet,
  MapOptions,
  marker,
  tileLayer
} from "leaflet";
import 'leaflet-routing-machine';
import {Route} from '../models/Route';
import {RoutesService} from '../services/data/routes/routes.service';
import {MatButton} from "@angular/material/button";
import {SidenavService} from "../services/app/sidenav/sidenav.service";
import {MapsService} from "../services/app/maps/maps.service";
import {first} from "rxjs/operators";
import {ItineraryAppService} from "../services/app/itinerary/itinerary-app.service";
import {SectionType} from "../services/data/itinerary/Section";

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})

export class MapsComponent implements AfterViewInit {
  @ViewChild('buttonOpenDrawer')
  matButton: MatButton | undefined

  @ViewChild('ButtonsZoom')
  zoomControl: ElementRef | undefined

  @ViewChild('logo')
  logo: ElementRef | undefined

  options: MapOptions = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {maxZoom: 18})
    ],
    zoomControl: false,
    zoom: 14,
    center: latLng(48.858353, 2.294464)
  };

  map: MapLeaflet | undefined
  layers: Layer [] = []

  markersLayer : LayerGroup = new LayerGroup()
  routesLayers: LayerGroup = new LayerGroup()
  departureLayers: LayerGroup = new LayerGroup()
  arrivalLayers: LayerGroup = new LayerGroup()
  itineraryLayer : LayerGroup = new LayerGroup();

  liveMarkersLayer : LayerGroup = new LayerGroup()
  routesLiveMarkersMap: Map<string, LayerGroup> = new Map<string, LayerGroup>();
  liveMarkersInProgressForRoute: Map<string, boolean> = new Map<string, boolean>();


  //For generating marker
  routes: Route [] = []

  constructor(
    @Inject(DOCUMENT) document: Document,
    private routesService: RoutesService,
    private itineraryAppService: ItineraryAppService,
    public sidenavService: SidenavService,
    private mapsService: MapsService
  ) {}

  ngAfterViewInit(): void {
    this.layers.push(this.routesLayers)
    this.layers.push(this.markersLayer)
    this.layers.push(this.liveMarkersLayer)
    this.layers.push(this.departureLayers)
    this.layers.push(this.arrivalLayers)
    this.layers.push(this.itineraryLayer)

    // Gestion des routes
    this.mapsService.routesSelectedEvent.subscribe((selectedRoutes) => {
      this.routesLayers.clearLayers()
      this.liveMarkersLayer.clearLayers()
      this.routesLiveMarkersMap.clear()
      this.routes = selectedRoutes
      this.routes.forEach(async route => {
        this.drawRoute(route)

        while (this.routes.find(r => r.routeId === route.routeId) && (!this.liveMarkersInProgressForRoute.has(route.routeId) || this.liveMarkersInProgressForRoute.get(route.routeId) === false)) {
          this.liveMarkersInProgressForRoute.set(route.routeId, true)
          await this.drawLiveMarkers(route.routeId)
          this.liveMarkersInProgressForRoute.set(route.routeId, false)
        }
      })
    })

    //Centrage sur le user
    this.mapsService.centerEventEmitter.subscribe(centerPosition => {
      this.map?.setView(centerPosition)
    })

    // Gestion des markers
    this.mapsService.markerEventEmitter.subscribe(markers => {
      this.markersLayer.clearLayers()
      markers.forEach(markerTab => {
        this.markersLayer.addLayer(marker(markerTab.positions, {icon: markerTab.divIcon}))
      })
    })

    this.itineraryAppService.departureEventEmitter.subscribe(departure => {
      this.departureLayers.clearLayers()
      const icon = divIcon({
        className: 'align-marker',
        html: document.getElementById('markerDeparture')?.innerHTML,
        iconSize: [18 * 2, 44 * 2],})

      // @ts-ignore
      this.departureLayers.addLayer(marker(departure.geometry['coordinates'].slice().reverse(), {icon}))
    })

    this.itineraryAppService.arrivalEventEmitter.subscribe(arrival => {
      this.arrivalLayers.clearLayers()
      const icon = divIcon({
        className: 'align-marker',
        html: document.getElementById('markerArrival')?.innerHTML,
        iconSize: [18 * 2, 44 * 2],})

      // @ts-ignore
      this.arrivalLayers.addLayer(marker(arrival.geometry['coordinates'].slice().reverse(), {icon}))
    })

    this.itineraryAppService.itineraryEventEmitter.subscribe(itinerary => {
      this.itineraryLayer.clearLayers()
      itinerary?.journeys[0]?.sections?.forEach(section => {
        switch (section.type) {
          case SectionType.PUBLIC_TRANSPORT:
            // this.itineraryLayer.addLayer(geoJSON(section.geojson))
            break;
          case SectionType.WALKING_STREET:
            // @ts-ignore
            this.itineraryLayer.addLayer(geoJSON(section.geojson, {dashArray: '4, 1, 2', dashOffset: '0'}))
            break;
          case SectionType.TRANSFER:
            this.itineraryLayer.addLayer(geoJSON(section.geojson))
            break;
        }
      })
    })
  }

  async drawRoute(route: Route) {
    this.routesLayers.addLayer(geoJSON(route.shapes, {style: {color: route.routeColor, opacity: 0.75, weight: 5}}))

    route.__stops__.forEach(stop => {
      const positions: LatLng = latLng(Number(stop.stopLat), Number(stop.stopLon))
      this.routesLayers.addLayer(circleMarker(positions, {
        radius: 5,
        color: 'black',
        weight: 1,
        fillColor: route.routeColor,
        fillOpacity: 1,
        opacity: 1
      }))
    })
  }

  drawLiveMarkers(routeId: string): Promise<void> {
    return new Promise(resolve => {
      this.routesService.getLive(routeId).pipe(first()).subscribe(featureCollection => {
        let layerGroup;
        if (this.routesLiveMarkersMap.has(routeId)) {
          layerGroup = this.routesLiveMarkersMap.get(routeId)
          layerGroup?.clearLayers()
        } else {
          layerGroup = new LayerGroup()
          this.routesLiveMarkersMap.set(routeId, layerGroup)
          this.liveMarkersLayer?.addLayer(layerGroup)
        }

        for (const feature of featureCollection.features) {
          if (document.getElementById(this.generateHTMLid(routeId))?.innerHTML !== undefined) {
            const icon = divIcon({
              className: 'align-marker',
              html: document.getElementById(this.generateHTMLid(routeId))?.innerHTML,
              iconSize: [18 * 2, 44 * 2],
            })

            // @ts-ignore
            const reversedCoord = feature.geometry['coordinates'].reverse()
            layerGroup?.addLayer(marker(reversedCoord, {icon: icon}))
          }
        }

        resolve()
      }, () => resolve())
    })
  }

  generateHTMLid(routeId: string) {
    return `ap-${routeId}`
  }

  onMapReady(mapLeaflet: MapLeaflet) {
    this.map = mapLeaflet
    this.map.attributionControl.remove()

    setTimeout(() => {
      let zoomButtons = new Control({position: 'bottomright'});
      zoomButtons.onAdd = () => {
        const div = this.zoomControl?.nativeElement
        DomEvent.disableClickPropagation(div)
        return div;
      };
      zoomButtons.addTo(mapLeaflet);

      let openMenuButton = new Control({position: 'topleft'});
      openMenuButton.onAdd = () => {
        const div = this.matButton?._elementRef.nativeElement
        DomEvent.disableClickPropagation(div)
        return div;
      };
      openMenuButton.addTo(mapLeaflet);

      let logo = new Control({position: 'topright'});
      logo.onAdd = () => {
        return this.logo?.nativeElement
      }
      logo.addTo(mapLeaflet)
    }, 0)
  }
}
