import {Component, OnInit} from '@angular/core';
import {ItineraryAppService} from "../services/app/itinerary/itinerary-app.service";
import {Itinerary, Journey} from "../services/data/itinerary/Itinerary";
import {LinkType, Section, SectionType} from "../services/data/itinerary/Section";
import {Route} from "../models/Route";
import {MapsService} from "../services/app/maps/maps.service";
import {RouteTypesService} from "../services/data/route-types/route-types.service";
import {PlaceType} from "../services/data/itinerary/Place";

@Component({
  selector: 'app-itinerary',
  templateUrl: './itinerary.component.html',
  styleUrls: ['./itinerary.component.css']
})
export class ItineraryComponent implements OnInit {
  waitingItinerary = true
  inLoading = false
  itinerary: Itinerary | undefined
  journey: Journey | undefined
  routes: Map<string, Route> = new Map<string, Route>();

  SectionType = SectionType
  PlaceType = PlaceType

  constructor(
    private itineraryAppService: ItineraryAppService,
    private routeTypesService: RouteTypesService,
    private mapsService: MapsService
  ) {}

  ngOnInit(): void {
    this.itineraryAppService.itineraryLoading.subscribe(inLoading => {
      this.inLoading = inLoading
    })

    this.itineraryAppService.itineraryEventEmitter.subscribe(itinerary => {
      this.routes.forEach(value => this.mapsService.removeRoute(value.routeId))
      this.routes.clear()

      this.waitingItinerary = false;
      this.inLoading = false
      this.itinerary = itinerary

      if (itinerary.journeys.length > 0) {
        this.journey = itinerary.journeys[0]
        const routeIds = this.findRouteIdInSection(this.journey.sections)

        routeIds.forEach(async routeId => {
          const route: Route = await this.mapsService.addRoute(routeId)
          this.routes.set(routeId, route)
        })
      }
    })
  }

  findRouteIdInSection(sections: Section[]): string[] {
    const routeIds: string[] = []

    sections.forEach(section => {
      if (section.type === SectionType.PUBLIC_TRANSPORT) {
        const lineId = section.links.find(link => link.type === LinkType.LINE)
        if (lineId) {
          routeIds.push(lineId.id.substring(5))
        }
      }
    })
    return routeIds
  }

  getSvgString(section: Section): string {
    let returnValue = ''
    const routeIds = this.findRouteIdInSection([section])

    if (routeIds.length === 1) {
      const route = this.routes.get(routeIds[0])
      if (route) {
        returnValue = this.routeTypesService.getIdSvgByRouteTypeId(route.routeType)
      }
    }

    return returnValue
  }
}
