import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {SidenavService} from "./services/app/sidenav/sidenav.service";
import {MatSidenav} from "@angular/material/sidenav";
import {LineChoicerComponent} from "./line-choicer/line-choicer.component";
import {MatDialog} from "@angular/material/dialog";
import {MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  @ViewChild('drawer') private drawer: MatSidenav | undefined ;

  constructor(public sidenavService: SidenavService, public dialog: MatDialog, private iconRegistry: MatIconRegistry, private  sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon('Bus', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/Bus.svg'));
    iconRegistry.addSvgIcon('Metro', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/Metro.svg'));
    iconRegistry.addSvgIcon('Navette', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/Navette.svg'));
    iconRegistry.addSvgIcon('Train2', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/Train2.svg'));
    iconRegistry.addSvgIcon('Tramway', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/Tramway.svg'));
    iconRegistry.addSvgIcon('CableCar', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/CableCar.svg'));
    iconRegistry.addSvgIcon('Rowing', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/Rowing.svg'));
    iconRegistry.addSvgIcon('Funicular', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/Funicular.svg'));
  }

  ngAfterViewInit(): void {
    this.sidenavService.setSidenav(this.drawer)
  }

  openModal(): void {
    this.dialog.open(LineChoicerComponent, {panelClass: ['w-4/5']});
  }
}
