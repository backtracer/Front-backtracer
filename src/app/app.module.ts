import {LOCALE_ID, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatChipsModule} from '@angular/material/chips';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MapsComponent } from './maps/maps.component';
import {LeafletModule} from "@asymmetrik/ngx-leaflet";
import { MarkerComponent } from './maps/marker/marker.component';
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatDividerModule} from "@angular/material/divider";
import {MatRippleModule} from "@angular/material/core";
import {DragDropModule} from '@angular/cdk/drag-drop';
import { HttpClientModule } from '@angular/common/http';
import {MatSelectModule} from "@angular/material/select";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatTooltipModule} from "@angular/material/tooltip";
import { MarkerUserPositionComponent } from './maps/marker/marker-user-position/marker-user-position.component';
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import { DepartureArrivalComponent } from './departure-arrival/departure-arrival.component';
import { LineChoicerComponent } from './line-choicer/line-choicer.component';
import {MatDialogModule} from "@angular/material/dialog";
import { RoutesTablesComponent } from './line-choicer/routes-tables/routes-tables.component';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatStepperModule} from "@angular/material/stepper";
import { ItineraryComponent } from './itinerary/itinerary.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { DurationPipe } from './pipe/duration.pipe';
import { MarkerDepartureComponent } from './maps/marker/marker-departure/marker-departure.component';
import { MarkerArrivalComponent } from './maps/marker/marker-arrival/marker-arrival.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
registerLocaleData(localeFr);


@NgModule({
    declarations: [
        AppComponent,
        MapsComponent,
        MarkerComponent,
        MarkerUserPositionComponent,
        DepartureArrivalComponent,
        LineChoicerComponent,
        RoutesTablesComponent,
        ItineraryComponent,
        DurationPipe,
        DurationPipe,
        MarkerDepartureComponent,
        MarkerArrivalComponent,
    ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LeafletModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatRippleModule,
    MatChipsModule,
    DragDropModule,
    HttpClientModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatInputModule,
    FormsModule,
    MatTooltipModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatCheckboxModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatStepperModule,
    MatProgressSpinnerModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: "fr-FR" }],
  bootstrap: [AppComponent]
})
export class AppModule { }
