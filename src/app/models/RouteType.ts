export type RouteType = {
  routeTypeId : number
  routeTypeName : string
  idSvg: string
}
