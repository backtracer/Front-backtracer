export type Stop = {
  stopId : string
  stopCode : string
  stopName : string
  stopDesc : string
  stopLat : string
  stopLon : string
  zoneId : string
  stopUrl : string
  locationType : number
  parentStation : string
  stopTimezone : string
  wheelchairBoarding : number
  platformCode : string
  direction : string
  position : string
}
