export type Agency = {
  id: string
  agencyId: string
  agencyName: string
  agencyUrl: string
  agencyTimezone: string
  agencyLang: string
  agencyPhone: string
  agencyFareUrl: string
  agencyEmail: string
}
