import {Agency} from "./Agency";
import {GeoJSONFeatureCollection} from "ol/format/GeoJSON";
import {Stop} from "./Stop";

export type Route = {
  routeId: string
  agencyId: string
  routeShortName: string
  routeLongName: string
  routeType: number
  routeColor: string
  routeTextColor: string
  shapes: GeoJSONFeatureCollection

  agency: Agency;
  __stops__: Stop[];
}
