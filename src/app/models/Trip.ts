import {StopTime} from "./StopTime";

export type Trip = {
  tripId: string
  routeId: string
  serviceId: string
  directionId: string
  tripHeadsign: string
  bikesAllowed: number
  wheelchairAccessible: number
  stopTimes: StopTime[]
}
