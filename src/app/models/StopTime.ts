export type StopTime = {
  tripId: string
  stopId: string
  stopSequence: number
  arrivalTime: string
  departureTime: string
  stopHeadsign: string
  pickupType: number
  dropOffType: number
  shapeDistTraveled: undefined
  timepoint: number
}
