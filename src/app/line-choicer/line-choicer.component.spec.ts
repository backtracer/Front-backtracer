import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LineChoicerComponent } from './line-choicer.component';

describe('LineChoicerComponent', () => {
  let component: LineChoicerComponent;
  let fixture: ComponentFixture<LineChoicerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LineChoicerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LineChoicerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
