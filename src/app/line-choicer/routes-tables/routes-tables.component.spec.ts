import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutesTablesComponent } from './routes-tables.component';

describe('RoutesTablesComponent', () => {
  let component: RoutesTablesComponent;
  let fixture: ComponentFixture<RoutesTablesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoutesTablesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutesTablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
