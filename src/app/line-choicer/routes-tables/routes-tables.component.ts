import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Route} from "../../models/Route";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MapsService} from "../../services/app/maps/maps.service";
import {MatCheckboxChange} from "@angular/material/checkbox";
import {FormControl, FormGroup} from "@angular/forms";
import {RouteTypesService} from "../../services/data/route-types/route-types.service";
import {AgenciesService} from "../../services/data/agencies/agencies.service";
import {Agency} from "../../models/Agency";
import {first, map, startWith} from "rxjs/operators";
import {RoutesService} from "../../services/data/routes/routes.service";
import {RouteType} from "../../models/RouteType";
import {Observable} from "rxjs";


export type RouteForTable = {
  selected: boolean;
  routeId: string;
  routeType: number
  routeLongName: string;
  agencyName: string
}

export type Filter = {
  routeType: number[],
  routeName: string,
  agencyName: string,
}

@Component({
  selector: 'app-routes-tables',
  templateUrl: './routes-tables.component.html',
  styleUrls: ['./routes-tables.component.css']
})
export class RoutesTablesComponent implements OnInit, AfterViewInit {
  // For table
  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;
  @ViewChild(MatSort) sort: MatSort | undefined;
  displayedColumns: string[] = ['selected', 'routeType', 'routeLongName', 'agencyName'];
  dataSource = new MatTableDataSource<RouteForTable>([]);

  // Filter Field
  formGroupFilter = new FormGroup({
    routeType: new FormControl([]),
    routeName: new FormControl(''),
    agencyName: new FormControl(''),
  })
  agencies: Agency[] = []
  agenciesFiltered: Observable<Agency[]> = new Observable<Agency[]>();

  routeTypes: RouteType[] = []
  loading = true

  constructor(
    private mapsService: MapsService,
    public routesService: RoutesService,
    public routeTypesService: RouteTypesService,
    public agenciesService: AgenciesService,
  ) {}

  ngOnInit(): void {
    this.formGroupFilter.valueChanges.subscribe(() => {
      this.dataSource.filter = JSON.stringify(this.formGroupFilter.value)
    })

    this.routesService.getRoutes({}).pipe(first()).subscribe(routes => {
      this.dataSource.data = routes.map((route: Route) => {
        return {
          selected: this.mapsService.isRouteSelected(route.routeId),
          routeId: route.routeId,
          routeType: route.routeType,
          routeLongName: route.routeLongName,
          agencyName: route.agency.agencyName
        }
      })

      this.loading = false
    })

    this.agenciesService.getAll().pipe(first()).subscribe(agencies => {
      this.agencies = agencies

      const formControl = this.formGroupFilter.get('agencyName');
      if (formControl) {
        this.agenciesFiltered = formControl.valueChanges.pipe(
          startWith(''),
          map(value => this.autocompleteAgencies(value)),
        );
      }
    })

    this.routeTypes = this.routeTypesService.getRouteTypes()

    this.dataSource.filterPredicate = (routeForTable: RouteForTable, filterString: string): boolean => {
      const filter: Filter = JSON.parse(filterString)

      const routeTypeBoolean = filter.routeType.length > 0 ?
        filter.routeType.includes(routeForTable.routeType)
        : true
      const routeLongNameBoolean = filter.routeName.trim().toLowerCase().length > 0 ?
        routeForTable.routeLongName.trim().toLowerCase().includes(filter.routeName.trim().toLowerCase())
        : true
      const routeAgencyNameBoolean = filter.agencyName.trim().toLowerCase().length > 0 ?
        routeForTable.agencyName.trim().toLowerCase().includes(filter.agencyName.trim().toLowerCase())
        : true

      return routeTypeBoolean && routeLongNameBoolean && routeAgencyNameBoolean
    }
  }

  ngAfterViewInit() {
    this.dataSource.sortingDataAccessor = (routeForTable: RouteForTable, columnDef) => {
      switch(columnDef) {
        case 'selected':
          return routeForTable.selected ? 1 : 0;
        default:
          // @ts-ignore
          return routeForTable[columnDef];
      }
    }

    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }

    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  }

  private autocompleteAgencies(value: string): Agency[] {
    const filterValue = value.toLowerCase();

    return this.agencies.filter(agency => agency.agencyName.toLowerCase().includes(filterValue));
  }

  onCheckboxChange(matCheckboxChange: MatCheckboxChange, routeForTable: RouteForTable): void {
    if (matCheckboxChange.checked) {
      routeForTable.selected = true
      this.mapsService.addRoute(routeForTable.routeId)
    } else {
      routeForTable.selected = false
      this.mapsService.removeRoute(routeForTable.routeId)
    }
  }

  findSvgWithRouteId(routeTypeId: number): string {
    const svgId = this.routeTypes.find(routeType => routeType.routeTypeId === routeTypeId)?.idSvg
    return svgId === undefined ? '' : svgId
  }
}
