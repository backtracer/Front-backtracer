import {Component} from '@angular/core';
import {FormControl} from "@angular/forms";
import {debounceTime, distinctUntilChanged, first, startWith, switchMap} from "rxjs/operators";
import {Observable} from "rxjs";
import {GeocodingService} from "../services/data/geocoding/geocoding.service";
import {GeoJSONFeature} from "ol/format/GeoJSON";
import {ItineraryAppService} from "../services/app/itinerary/itinerary-app.service";
import {UserPositionService} from "../services/app/user-position/user-position.service";
import {MatAutocompleteSelectedEvent} from "@angular/material/autocomplete";

@Component({
  selector: 'app-departure-arrival',
  templateUrl: './departure-arrival.component.html',
  styleUrls: ['./departure-arrival.component.css']
})
export class DepartureArrivalComponent {

  departureControl = new FormControl()
  arrivalControl = new FormControl()
  filteredOptionsDeparture: Observable<any>;
  filteredOptionsArrival: Observable<any>;

  constructor(
    private geocodingService: GeocodingService,
    private departureArrivalService: ItineraryAppService,
    public userPositionService: UserPositionService
  ) {
    this.filteredOptionsDeparture = this.departureControl.valueChanges
      .pipe(
        startWith(''),
        debounceTime(5),
        distinctUntilChanged(),
        switchMap(val => {
          return this.filter(val || '')
        })
      );

    this.filteredOptionsArrival = this.arrivalControl.valueChanges
      .pipe(
        startWith(''),
        debounceTime(5),
        distinctUntilChanged(),
        switchMap(val => {
          return this.filter(val || '')
        })
      );
  }

  filter(val: string): any {
    // val not always string cause of matAutocomplete
    // and if removed the last request look like https://localhost:3000/autocomplete?q=[Object object]
    if (val && typeof val === 'string') {
      return new Promise(resolve => {
        this.geocodingService.autocomplete(val)
          .pipe(first())
          .subscribe((response) => {
            resolve(response.features)
          }, () => {
            resolve([])
          })
      })
    } else {
      return []
    }
  }

  displayAddress(geoJsonFeature: GeoJSONFeature): string {
    return geoJsonFeature?.properties?.label ? geoJsonFeature.properties.label : '';
  }

  onDepartureSelected(matAutocompleteSelectedEvent: MatAutocompleteSelectedEvent): void {
    return this.departureArrivalService.setDeparture(matAutocompleteSelectedEvent.option.value)
  }

  onArrivalSelected(matAutocompleteSelectedEvent: MatAutocompleteSelectedEvent): void {
    this.departureArrivalService.setArrival(matAutocompleteSelectedEvent.option.value)
  }
}
