import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartureArrivalComponent } from './departure-arrival.component';

describe('TravelChoicerComponent', () => {
  let component: DepartureArrivalComponent;
  let fixture: ComponentFixture<DepartureArrivalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepartureArrivalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartureArrivalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
