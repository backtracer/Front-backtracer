import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {Observable} from "rxjs";
import {GeoJSONFeatureCollection} from "ol/format/GeoJSON";

@Injectable({
  providedIn: 'root'
})
export class GeocodingService {

  constructor(private http: HttpClient) { }

  autocomplete(q: string): Observable<GeoJSONFeatureCollection> {
    const params = new HttpParams()
      .set('q', q)

    return this.http.get<GeoJSONFeatureCollection>(`${environment.api_link}/geocoding/autocomplete`, {params});
  }
}
