import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Agency} from "../../../models/Agency";
import {environment} from "../../../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AgenciesService {
  constructor(private http: HttpClient) { }

  getAll(): Observable<Agency[]> {
    return this.http.get<Agency[]>(`${environment.api_link}/agencies`)
  }
}
