import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import { GeoJSONFeatureCollection } from 'ol/format/GeoJSON';
import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment';
import {Route} from '../../../models/Route';

@Injectable({
  providedIn: 'root'
})
export class RoutesService {
  constructor(private http: HttpClient) {
  }

  getRoutes(params: any = {}): Observable<Route[]> {
    return this.http.get<Route[]>(`${environment.api_link}/routes`, {params})
  }

  getRouteById(routeId: string): Observable<Route> {
    return this.http.get<Route>(`${environment.api_link}/routes/${routeId}`)
  }

  getLive(idRoute: string): Observable<GeoJSONFeatureCollection> {
    return this.http.get<GeoJSONFeatureCollection>(`${environment.api_link}/routes/${idRoute}/live`)
  }
}
