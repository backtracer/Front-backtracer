import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Itinerary} from "./Itinerary";

@Injectable({
  providedIn: 'root'
})
export class ItineraryService {

  constructor(private http: HttpClient) { }

  getItinerary(params: any = {}): Observable<Itinerary> {
    return this.http.get<Itinerary>(`${environment.api_link}/itinerary/`, {params})
  }
}
