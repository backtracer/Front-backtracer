export type Place = PlaceStopPoint | PlaceAddress;

export enum PlaceType {
  STOP_POINT = 'stop_point',
  ADDRESS = 'address',
}

export interface PlaceStopPoint {
  embedded_type: PlaceType.STOP_POINT
  name: string
  stop_point: StopPoint
}

export interface PlaceAddress {
  embedded_type: PlaceType.ADDRESS
  name: string
}

export interface StopPoint {
  name: string
}
