import {Place} from "./Place";
import {GeoJSONObject} from "ol/format/GeoJSON";

export type Section = SectionWaiting | SectionPublicTransport | SectionWalkingStreet | SectionTransfer

export enum SectionType {
  WALKING_STREET = 'street_network',
  TRANSFER = 'transfer',
  WAITING = 'waiting',
  PUBLIC_TRANSPORT = 'public_transport'
}

export interface SectionPublicTransport {
  type: SectionType.PUBLIC_TRANSPORT
  departure_date_time: string
  arrival_date_time: string
  duration: number // in seconds
  geojson: GeoJSONObject
  from: Place
  to: Place
  links: Link[]
  display_informations: {code: string, direction: string, color: string, text_color: string, trip_short_name: string}
}

export interface SectionWaiting {
  type: SectionType.WAITING
  departure_date_time: string
  arrival_date_time: string
  duration: number // in seconds
}

export interface SectionTransfer {
  type: SectionType.TRANSFER
  departure_date_time: string
  arrival_date_time: string
  duration: number // in seconds
  geojson: GeoJSONObject
  from: Place
  to: Place
}

export interface SectionWalkingStreet {
  type: SectionType.WALKING_STREET
  departure_date_time: string
  arrival_date_time: string
  duration: number // in seconds
  geojson: GeoJSONObject
  from: Place
  to: Place
}

export type Link = LinkLine

export enum LinkType {
  LINE = 'line'
}

export interface LinkLine {
  id: string // line:IDFM:C02317
  type: LinkType.LINE
}
