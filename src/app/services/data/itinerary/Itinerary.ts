import {Section} from "./Section";

export interface Itinerary {
  journeys: Journey[];
  tickets: any[]; // Not use
  links: any[]; // Not use
  disruptions: any[]; // Not use
}

export interface Journey {
  departure_date_time:	string
  arrival_date_time:	string
  duration: number; // in seconds
  sections: Section[]
}
