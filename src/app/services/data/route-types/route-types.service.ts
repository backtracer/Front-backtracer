import { Injectable } from '@angular/core';
import {RouteType} from "../../../models/RouteType";

@Injectable({
  providedIn: 'root'
})
export class RouteTypesService {

  routeTypes: RouteType[] = []

  constructor() {
    this.routeTypes = [
      {routeTypeId : 0, routeTypeName : 'Tram, Streetcar, Light rail', idSvg: 'Tramway'},
      {routeTypeId : 1, routeTypeName : 'Subway, Metro', idSvg: 'Metro'},
      {routeTypeId : 2, routeTypeName : 'Rail', idSvg: 'Train2'},
      {routeTypeId : 3, routeTypeName : 'Bus', idSvg: 'Bus'},
      {routeTypeId : 4, routeTypeName : 'Ferry', idSvg: 'Navette'},
      {routeTypeId : 5, routeTypeName : 'Cable car', idSvg: 'CableCar'},
      {routeTypeId : 6, routeTypeName : 'Gondola, Suspended cable car', idSvg: 'Rowing'},
      {routeTypeId : 7, routeTypeName : 'Funicular', idSvg: 'Funicular'}
    ]
  }

  getRouteTypes() : RouteType[] {
    return this.routeTypes
  }

  getIdSvgByRouteTypeId(routeTypeId: number): string {
      const idSvg = this.routeTypes.find(routeType => routeType.routeTypeId === routeTypeId)?.idSvg
      return idSvg ? idSvg : ''
  }
}
