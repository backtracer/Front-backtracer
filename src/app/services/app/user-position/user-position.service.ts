import { Injectable } from '@angular/core';
import {MapsService} from "../maps/maps.service";
import {divIcon, latLng} from "leaflet";
import {GeoJSONFeature} from "ol/format/GeoJSON";

@Injectable({
  providedIn: 'root'
})
export class UserPositionService {

  alreadyCentered: boolean = false
  userPositionAccepted = false
  userPosition: [number, number] = [0,0]

  constructor(private mapsService : MapsService) {
    setTimeout(() => {
      const icon = divIcon({
        className: 'align-marker',
        html: document.getElementById('userPositionMarker')?.innerHTML,
        iconSize: [18 * 2, 44 * 2],})

      navigator.geolocation.watchPosition((position) => {
        this.userPosition = [position.coords.longitude, position.coords.latitude]
        this.userPositionAccepted = true

        const userPosition = latLng(position.coords.latitude, position.coords.longitude)
          if(!this.alreadyCentered){
            this.mapsService.setCenter(userPosition)
            this.alreadyCentered = true
          }
          this.mapsService.addMarker('user-icon', icon, userPosition)
        },
        function errorCallback(error) {
          alert('ERROR(' + error.code + '): ' + error.message);
        })
    }, 0)
  }

  getUserPosition() {
    return this.userPosition
  }

  getUserPositionGeoJSON(): GeoJSONFeature {
    return {
      properties: {
        label: 'Ma position'
      },
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: this.getUserPosition()
      }
    }
  }

  isUserPositionAccepted(): boolean {
    return this.userPositionAccepted
  }
}
