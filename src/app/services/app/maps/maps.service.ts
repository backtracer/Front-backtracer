import {EventEmitter, Injectable} from '@angular/core';
import {DivIcon, LatLngExpression} from "leaflet";
import {Route} from "../../../models/Route";
import {RoutesService} from "../../data/routes/routes.service";
import {first} from "rxjs/operators";

export interface MarkerForMap {
  positions: LatLngExpression,
  divIcon: DivIcon
}

@Injectable({
  providedIn: 'root'
})
export class MapsService {

  routesSelected: Map<string, Route> = new Map<string, Route>()
  routesSelectedEvent: EventEmitter<Route[]> = new EventEmitter<Route[]>()

  markers: Map<string, MarkerForMap> = new Map<string, MarkerForMap>()
  markerEventEmitter : EventEmitter<MarkerForMap[]> = new EventEmitter<MarkerForMap[]>()

  centerEventEmitter: EventEmitter<LatLngExpression> = new EventEmitter<LatLngExpression>()
  itineraryEventEmitter : EventEmitter<any> = new EventEmitter<any>()

  constructor(private routesService: RoutesService) {}

  setCenter(positions: LatLngExpression) {
    this.centerEventEmitter.emit(positions)
  }

  addMarker(id: string, divIcon: DivIcon, positions: LatLngExpression): void {
    this.markers.set(id, {divIcon, positions})
    this.markerEventEmitter.emit(Array.from(this.markers.values()))
  }

  removeMarker(id: string): void {
    this.markers.delete(id)
    this.markerEventEmitter.emit(Array.from(this.markers.values()))
  }

  addRoute(routeId: string): Promise<Route> {
    return new Promise<Route>(resolve => {
      this.routesService.getRouteById(routeId).pipe(first()).subscribe(route => {
        this.routesSelected.set(routeId, route)
        this.routesSelectedEvent.emit(Array.from(this.routesSelected.values()))
         resolve(route)
      })
    })
  }

  removeRoute(routeId: string): void {
    this.routesSelected.delete(routeId)
    this.routesSelectedEvent.emit(Array.from(this.routesSelected.values()))
  }

  isRouteSelected(routeId: string): boolean {
    return this.routesSelected.has(routeId)
  }
}
