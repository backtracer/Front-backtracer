import { TestBed } from '@angular/core/testing';

import { ItineraryAppService } from './itinerary-app.service';

describe('DepartureArrivalService', () => {
  let service: ItineraryAppService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItineraryAppService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
