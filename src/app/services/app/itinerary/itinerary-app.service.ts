import {EventEmitter, Injectable} from '@angular/core';
import {GeoJSONFeature} from "ol/format/GeoJSON";
import {ItineraryService} from "../../data/itinerary/itinerary.service";
import {first} from "rxjs/operators";
import {Itinerary} from "../../data/itinerary/Itinerary";

@Injectable({
  providedIn: 'root'
})
export class ItineraryAppService {

  departure: GeoJSONFeature | undefined
  arrival: GeoJSONFeature | undefined

  itineraryEventEmitter: EventEmitter<Itinerary> = new EventEmitter<Itinerary>()
  itineraryLoading: EventEmitter<boolean> = new EventEmitter<boolean>()
  departureEventEmitter: EventEmitter<GeoJSONFeature> = new EventEmitter<GeoJSONFeature>()
  arrivalEventEmitter: EventEmitter<GeoJSONFeature> = new EventEmitter<GeoJSONFeature>()

  constructor(private itineraryService: ItineraryService) {}

  setDeparture(departure: GeoJSONFeature): void {
    this.departure = departure
    this.departureEventEmitter.emit(departure)
    this.emitItinerary()
  }

  setArrival(arrival: GeoJSONFeature): void {
    this.arrival = arrival
    this.arrivalEventEmitter.emit(arrival)
    this.emitItinerary()
  }

  emitItinerary(): void {
    if (this.departure && this.arrival) {
      this.itineraryLoading.emit(true)

      // @ts-ignore
      const departure = `${this.departure.geometry['coordinates'][0]};${this.departure.geometry['coordinates'][1]}`
      // @ts-ignore
      const arrival = `${this.arrival.geometry['coordinates'][0]};${this.arrival.geometry['coordinates'][1]}`

      this.itineraryService.getItinerary({departure, arrival}).pipe(first()).subscribe(itinerary => {
        this.itineraryEventEmitter.emit(itinerary)
      }, () => {
        this.itineraryEventEmitter.emit({disruptions: [], links: [], tickets: [], journeys: []})
      })
    }
  }
}
